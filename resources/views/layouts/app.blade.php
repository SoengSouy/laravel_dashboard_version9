<!DOCTYPE html>
<html lang="en">
<head>
	<title>soengsouy.com</title>

	<!-- Meta -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<meta name="description" content="codedthemes">
	<meta name="keywords"content=", Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
	<meta name="author" content="codedthemes">

	<!-- Favicon icon -->
	<link rel="shortcut icon" href="{{URL::to('assets/images/favicon.png" type="image/x-icon')}}">
	<link rel="icon" href="{{URL::to('assets/images/favicon.ico" type="image/x-icon')}}">
	<!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Ubuntu:400,500,700" rel="stylesheet">
	<!-- Font Awesome -->
	<link href="{{URL::to('assets/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
	<!--ico Fonts-->
	<link rel="stylesheet" type="text/css" href="{{URL::to('assets/icon/icofont/css/icofont.css')}}">
    <!-- Required Fremwork -->
    <link rel="stylesheet" type="text/css" href="{{URL::to('assets/plugins/bootstrap/css/bootstrap.min.css')}}">
	<!-- waves css -->
	<link rel="stylesheet" type="text/css" href="{{URL::to('assets/plugins/Waves/waves.min.css')}}">
	<!-- Style.css -->
	<link rel="stylesheet" type="text/css" href="{{URL::to('assets/css/main.css')}}">
	<!-- Responsive.css-->
	<link rel="stylesheet" type="text/css" href="{{URL::to('assets/css/responsive.css')}}">
	<!--color css-->
	<link rel="stylesheet" type="text/css" href="{{URL::to('assets/css/color/color-1.min.css')}}" id="color"/>

</head>
<body>
    <style>
        .invalid-feedback{
            color:red;
        }
    </style>

    {{-- content --}}
    @yield('content')
    <!-- Warning Section Starts -->

    <!-- Required Jqurey -->
    <script src="{{URL::to('assets/plugins/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{URL::to('assets/plugins/jquery-ui/jquery-ui.min.js')}}"></script>
    <script src="{{URL::to('assets/plugins/tether/dist/js/tether.min.js')}}"></script>

    <!-- Required Fremwork -->
    <script src="{{URL::to('assets/plugins/bootstrap/js/bootstrap.min.js')}}"></script>

    <!-- waves effects.js -->
    <script src="{{URL::to('assets/plugins/Waves/waves.min.js')}}"></script>
    <!-- Custom js -->
    <script type="text/javascript" src="{{URL::to('assets/pages/elements.js')}}"></script>

</body>
</html>