@extends('layouts.app')
@section('content')
    <section class="login p-fixed d-flex text-center bg-primary common-img-bg">
        <!-- Container-fluid starts -->
        <div class="container-fluid">
            <div class="row">

                <div class="col-sm-12">
                    <div class="login-card card-block">
                        <form method="POST" action="{{ route('login') }}" class="md-float-material">
                            @csrf
                            <div class="text-center">
                                <img src="{{URL::to('assets/images/logo-black.png')}}" alt="logo">
                            </div>
                            <h3 class="text-center txt-primary">
                                Sign In to your account
                            </h3>
                            @if(session()->has('error'))
                                <div class="text-danger text-center">
                                    {{ session()->get('error') }}
                                </div>
                            @endif
            
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="md-input-wrapper">
                                        <input id="email" type="email" class="md-form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" autocomplete="email" autofocus placeholder="Enter email">
                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                        <label>Email</label>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="md-input-wrapper">
                                        <input id="password" type="password" class="md-form-control @error('password') is-invalid @enderror" name="password" autocomplete="current-password" placeholder="Enter password">
                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                        <label>Password</label>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                <div class="rkmd-checkbox checkbox-rotate checkbox-ripple m-b-25">
                                    <label class="input-checkbox checkbox-primary">
                                        <input type="checkbox" id="checkbox">
                                        <span class="checkbox"></span>
                                    </label>
                                    <div class="captions">Remember Me</div>

                                </div>
                                    </div>
                                <div class="col-sm-6 col-xs-12 forgot-phone text-right">
                                    <a href="{{ route('forget-password') }}" class="text-right f-w-600"> Forget Password?</a>
                                    </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-10 offset-xs-1">
                                    <button type="submit" class="btn btn-primary btn-md btn-block waves-effect text-center m-b-20">LOGIN</button>
                                </div>
                            </div>
                            <!-- <div class="card-footer"> -->
                            <div class="col-sm-12 col-xs-12 text-center">
                                <span class="text-muted">Don't have an account?</span>
                                <a href="{{route('register')}}" class="f-w-600 p-l-5">Sign up Now</a>
                            </div>

                            <!-- </div> -->
                        </form>
                        <!-- end of form -->
                    </div>
                    <!-- end of login-card -->
                </div>
                <!-- end of col-sm-12 -->
            </div>
            <!-- end of row -->
        </div>
        <!-- end of container-fluid -->
    </section>
@endsection