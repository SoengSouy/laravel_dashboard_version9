
@extends('layouts.app')
@section('content')
	<section class="login common-img-bg">
		<!-- Container-fluid starts -->
		<div class="container-fluid">
			<div class="row">
                <div class="col-sm-12">
                    <div class="login-card card-block bg-white">
                        <form method="POST" action="{{ route('register') }}" class="md-float-material">
                            @csrf
                            <div class="text-center">
                                <img src="assets/images/logo-black.png" alt="logo">
                            </div>
                            <h3 class="text-center txt-primary">Create an account </h3>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="md-input-wrapper">
                                        <input id="name" type="text" class="md-form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" autocomplete="name" autofocus  placeholder="Username">
                                        @error('name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                        <label>Full Name</label>
                                    </div>
                                </div>
                            </div>
                            <div class="md-input-wrapper">
                                <input id="email" type="email" class="md-form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" autocomplete="email" placeholder="Enter email">
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                <label>Email Address</label>
                            </div>
                            <div class="md-input-wrapper">
                                <input id="password" type="password" class="md-form-control @error('password') is-invalid @enderror" name="password" autocomplete="new-password" placeholder="Enter password">
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                <label>Password</label>
                            </div>
                            <div class="md-input-wrapper">
                                <input type="password" class="md-form-control" name="password_confirmation">
                                <label>Confirm Password</label>
                            </div>

                            <div class="rkmd-checkbox checkbox-rotate checkbox-ripple b-none m-b-20">
                                <label class="input-checkbox checkbox-primary">
                                    <input type="checkbox" id="checkbox">
                                    <span class="checkbox"></span>
                                </label>
                                <div class="captions">Remember Me</div>
                            </div>
                            <div class="col-xs-10 offset-xs-1">
                                <button type="submit" class="btn btn-primary btn-md btn-block waves-effect waves-light m-b-20">Sign up
                                </button>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 text-center">
                                    <span class="text-muted">Already have an account?</span>
                                    <a href="{{ route('login') }}" class="f-w-600 p-l-5"> Sign In Here</a>

                                </div>
                            </div>
                        </form>
                        <!-- end of form -->
                    </div>
                    <!-- end of login-card -->
                </div>
                <!-- end of col-sm-12 -->
            </div>
            <!-- end of row-->
        </div>
        <!-- end of container-fluid -->
	</section>
    @endsection
